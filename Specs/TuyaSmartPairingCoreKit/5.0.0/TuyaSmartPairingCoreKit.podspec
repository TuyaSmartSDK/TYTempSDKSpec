
Pod::Spec.new do |s|
  s.name             = 'TuyaSmartPairingCoreKit'
  s.version          = '5.0.0'
  s.summary          = 'A short description of TuyaSmartPairingCoreKit'
  s.homepage         = 'https://tuya.com'
  s.license          = { :type => 'MIT' }
  s.author           = { 'iosdeveloper' => 'iosdeveloper@tuya.com' }
  s.source           = { :http => "https://raw.githubusercontent.com/huangdaxiaEX/TYDemoZip/master/TuyaSmartPairingCoreKit.zip", :type => "zip" }

  s.ios.deployment_target     = '9.0'
  s.static_framework          = true

  s.source_files = 'TuyaSmartPairingCoreKit/**/*.{h,m,mm,c}'
  s.module_map = 'TuyaSmartPairingCoreKit/module.modulemap'
  s.public_header_files = 'TuyaSmartPairingCoreKit/**/*.h'

  s.user_target_xcconfig = { 'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES' }
  s.pod_target_xcconfig = { 'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES' }
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES' }

  s.dependency 'ThingSmartPairingCoreKit'
end
